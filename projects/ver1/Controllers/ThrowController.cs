using Microsoft.AspNetCore.Mvc;

namespace ver1.Controllers;

[ApiController]
[Route("[controller]")]
public class ThrowController : ControllerBase
{
    private readonly ILogger<WeatherForecastController> _logger;

    public ThrowController(ILogger<WeatherForecastController> logger)
    {
        _logger = logger;
    }

    [HttpGet(Name = "throw")]
    public string Get()
    {
        ThrowService.Fall = true;
        return "Ok";
    }
}
